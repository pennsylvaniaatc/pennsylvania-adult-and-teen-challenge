Pennsylvania Adult and Teen Challenge provides faith-based addiction treatment to teens and adults across Pennsylvania and the United States. Through residential and outpatient treatment programs, we offer comprehensive support for lifelong recovery from drug and alcohol addiction.

Website: https://www.paatc.org/
